\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cv0}[2014/01/01 v 0.1 CV]

\LoadClass{scrartcl}

\RequirePackage{graphicx}
\RequirePackage[nochapters]{classicthesis} % Use the classicthesis style for the style of the document
\RequirePackage[LabelsAligned]{currvita} % Use the currvita style for the layout of the document
\RequirePackage{hyperref} % Required for adding links	and customizing them
\hypersetup{	colorlinks, 
			breaklinks, 
			urlcolor=red,
		 	linkcolor=red
			} % Set link colors


\newlength{\datebox}\settowidth{\datebox}{Spring 2011}

\newcommand{\NewEntry}[3] {
	\noindent
	\hangindent=2em
	\hangafter=0 
	\parbox{\datebox} {
		\small 
		\textit{#1}
		}
	\hspace {1.5em} #2 #3 \vspace{0.5em}
	}
	
\newcommand{\Description}[1] {
	\hangindent=2em
	\hangafter=0
	\noindent
	\raggedright
	\footnotesize{#1}
	\par
	\normalsize
	\vspace{1em}
	} 
	
\renewcommand{\cvheadingfont} {
	\LARGE
	\color{Maroon}
	}
	
\newcommand{\MarginText}[1] {
	\marginpar {
		\raggedleft
		\itshape
		\small#1
		}
	} 
	
\newcommand{\personal} {
	\noindent
	\input{sections/personal}
	\vspace{1em} 
	}
	
\newcommand{\education} {
	\noindent
	\input{sections/edu}
	\vspace{1em} 
	}
	
\newcommand{\workxp} {
	\noindent
	\input{sections/work}
	\vspace{1em} 
	}
	
\newcommand{\publication} {
	\noindent
	\input{sections/pub}
	\vspace{1em} 
	}
	
\newcommand{\otherinfo} {
	\noindent
	\input{sections/other}
	\vspace{1em} 
	}
	
\newcommand{\computerskill} {
	\noindent
	\input{sections/computer}
	\vspace{1em} 
	}


