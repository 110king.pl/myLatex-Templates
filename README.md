myLatex - Template Collection v0.1
===================================
This is a collection of Latex template files I found and edited.

Main features :
- easy to use
- multiple files for  sections / chapters / cover / abstract
- figure / section /chapter is in seperate folders
- all useful packages are included
- a sample of each package is included
