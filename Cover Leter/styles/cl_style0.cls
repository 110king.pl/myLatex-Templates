% Original author:
% Rensselaer Polytechnic Institute 
% http://www.rpi.edu/dept/arc/training/latex/resumes/
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cl0}[2014/01/01 v 0.1 CL]


\LoadClass[11pt]{letter} % Default font size of the document, change to 10pt to fit more text

\RequirePackage{newcent} % Default font is the New Century Schoolbook PostScript font 
%\RequirePackage{helvet} % Uncomment this (while commenting the above line) to use the Helvetica font

% Margins
\topmargin=-1in % Moves the top of the document 1 inch above the default
\textheight=8.5in % Total height of the text on the page before text goes on to the next page, this can be increased in a longer letter
\oddsidemargin=-10pt % Position of the left margin, can be negative or positive if you want more or less room
\textwidth=6.5in % Total width of the text, increase this if the left margin was decreased and vice-versa

\let\raggedleft\raggedright % Pushes the date (at the top) to the left, comment this line to have the date on the right

\RequirePackage{hyperref} % Required for adding links	and customizing them
\hypersetup{	colorlinks, 
			breaklinks, 
			urlcolor=red,
		 	linkcolor=red
			} % Set link colors
	
\newcommand{\cToWhom} {
	\noindent
	\input{sections/toWhom}
	\vspace{1em} 
	}
	
\newcommand{\cMyAddress} {
	\noindent
	\input{sections/myAddress}
	\vspace{1em} 
	}
	
\newcommand{\cLetterContent} {
	\noindent
	\input{sections/letterContent}
	\vspace{1em} 
	}
